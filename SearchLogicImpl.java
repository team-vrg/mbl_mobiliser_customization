package com.sybase365.mobiliser.custom.project.businesslogic.impl;

import com.sybase365.mobiliser.custom.project.businesslogic.ISearchLogic;
import com.sybase365.mobiliser.custom.project.businesslogic.configuration.CustomerUtil;
import com.sybase365.mobiliser.custom.project.businesslogic.impl.util.CustomConstants;
import com.sybase365.mobiliser.custom.project.businesslogic.impl.util.CustomUtil;
import com.sybase365.mobiliser.custom.project.converter.IConsumerConverter;
import com.sybase365.mobiliser.custom.project.converter.ICustomerInformationConverter;
import com.sybase365.mobiliser.custom.project.converter.ISearchResultConverter;
import com.sybase365.mobiliser.custom.project.persistence.dao.factory.api.DaoFactory;
import com.sybase365.mobiliser.custom.project.persistence.model.AgentPartner;
import com.sybase365.mobiliser.custom.project.persistence.model.AgentReceipts;
import com.sybase365.mobiliser.custom.project.persistence.model.ConsumerDetails;
import com.sybase365.mobiliser.custom.project.persistence.model.CustomerInformation;
import com.sybase365.mobiliser.custom.project.services.contract.v1_0.beans.*;
import com.sybase365.mobiliser.money.businesslogic.util.EntityNotFoundException;
import com.sybase365.mobiliser.money.businesslogic.wallet.IPILogic;
import com.sybase365.mobiliser.money.converter.customer.ICustomerConverter;
import com.sybase365.mobiliser.money.converter.umgrroles.IUmgrConverter;
import com.sybase365.mobiliser.money.converter.wallet.IPIConverter;
import com.sybase365.mobiliser.money.persistence.model.customer.Customer;
import com.sybase365.mobiliser.money.persistence.model.customer.CustomerAttribute;
import com.sybase365.mobiliser.money.persistence.model.customer.CustomerRole;
import com.sybase365.mobiliser.money.persistence.model.pi.BankAccount;
import com.sybase365.mobiliser.money.persistence.model.pi.PaymentInstrument;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.expression.ExpressionException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Imran.Tariq
 *
 */
@Transactional
public class SearchLogicImpl implements InitializingBean, ISearchLogic {

  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(SearchLogicImpl.class);

  protected DaoFactory daoFactory;

  private com.sybase365.mobiliser.money.persistence.dao.factory.api.DaoFactory moneyDaoFactory;

  private ICustomerConverter customerConverter;

  private IUmgrConverter umgrConverter;

  private IConsumerConverter consumerConverter;

  protected CustomUtil customUtil;

  private ISearchResultConverter searchResultConverter;

  private IPIConverter piConverter;

  private IPILogic piLogic;

  private ICustomerInformationConverter customerInformationConverter;

    private CustomerUtil customerUtil;

  public DaoFactory getDaoFactory() {
    return daoFactory;
  }

  public void setDaoFactory(DaoFactory daoFactory) {
    this.daoFactory = daoFactory;
  }

  public CustomUtil getCustomUtil() {
    return customUtil;
  }

  public void setCustomUtil(CustomUtil customUtil) {
    this.customUtil = customUtil;
  }

    public CustomerUtil getCustomerUtil() {
        return customerUtil;
    }

    public void setCustomerUtil(CustomerUtil customerUtil) {
        this.customerUtil = customerUtil;
    }

    public ISearchResultConverter getSearchResultConverter() {
    return searchResultConverter;
  }

  public void setSearchResultConverter(ISearchResultConverter searchResultConverter) {
    this.searchResultConverter = searchResultConverter;
  }

  /**
   * This method takes searchCriteria and return list of customers searched by searchCriteria.
   * 
   */
  @Override
  public List<CustomerSearchResultCBean> getAgentsList(SearchCriteriaBean searchCriteria, long callerId, String orgUnit)
      throws EntityNotFoundException {

	    List<CustomerSearchResultCBean> customers = null;
	    try {
          String firstName = searchCriteria.getUsername();
          String lastName = "";
          String accountNumber = "";
          String msisdn = searchCriteria.getMsisdn();
          // to convert into international format to search with 0 or +92
          msisdn = CustomUtil.convertMsisdnForSearch(msisdn, "92");
          String cnic = searchCriteria.getCnic();
          // Registration type, e.g.: 'Agent', 'Guest', 'Customer' etc.
          String registrationType = "";
          String agentID = null;
          if (searchCriteria.getAgentId() != null)
            agentID = String.valueOf(searchCriteria.getAgentId());

          LOG.info("first name: " + firstName + " |last name: " + lastName + " |account number: " + accountNumber
                  + " |msisdn: " + msisdn + " |cnic: " + cnic + " |agentID: " + agentID);
          // flag to identify if either first or last name has been used for
          // search
          boolean isNamePresent = StringUtils.hasText(firstName) || StringUtils.hasText(lastName);
          boolean isIdentifactionPresent = StringUtils.hasText(msisdn) || StringUtils.hasText(accountNumber);
          // boolean to check if the query has been modified by adding clauses
          boolean isWhereClauseModified = false;

          StringBuilder query = new StringBuilder();
          // Main select clause
          query.append("select cust, '123' FROM com.sybase365.mobiliser.money.persistence.model.customer.Customer cust");

          //PI
//	      query.append(" , com.sybase365.mobiliser.money.persistence.model.pi.PaymentInstrument pi");
//	      query.append(" , com.sybase365.mobiliser.money.persistence.model.pi.WalletEntry wallet");

          // if first/last name used as search criteria
          if (isNamePresent) {
            // isSearchCriteriaPresent = true;
            query.append(" , com.sybase365.mobiliser.money.persistence.model.customer.Address addr");
          }
          // if msisdn or account number used as search criteria
          if (StringUtils.hasText(msisdn) || StringUtils.hasText(accountNumber)) {
            // isSearchCriteriaPresent = true;
            query.append(" , com.sybase365.mobiliser.money.persistence.model.customer.Identification iden");
          }

          if (StringUtils.hasText(cnic)) {
            query.append(" , com.sybase365.mobiliser.money.persistence.model.customer.Identity identity");
          }

          if (StringUtils.hasText(agentID)) {
            query.append(" ,  com.sybase365.mobiliser.custom.project.persistence.model.ConsumerDetails consDeatils ");
          }

          query.append(" where");

          if (StringUtils.hasText(agentID)) {
            query.append(" cust.id = consDeatils.customer.id AND");
          }
                //cnic
          if (StringUtils.hasText(cnic)) {
              if(searchCriteria.getCustomerType().equals("52")) {
                  query.append(" cust.id = identity.customer.id");      //ntn
                  query.append(" AND identity.identity = '" + cnic + "' AND identity.type='4' AND identity.customer.orgUnit.id = '" + orgUnit +"'");

              } else {      //cnic
                  query.append(" cust.id = identity.customer.id");
                  query.append(" AND identity.identity = '" + cnic + "' AND identity.type='2' AND identity.customer.orgUnit.id = '" + orgUnit + "'");
              }
            isWhereClauseModified = true;
          }

          // Now add the join conditions
          if (isNamePresent) {
            if (isWhereClauseModified) {
              query.append(" AND cust.id = addr.customer.id");
            }
            else {
              query.append(" cust.id = addr.customer.id");
            }

            // if first name present
            if (StringUtils.hasText(firstName)) {
              // if first name is present then add it
              query.append(" AND lower(addr.firstName) like '");
              query.append("%"+firstName.toLowerCase()+"%" + "'");
            }

            if (StringUtils.hasText(lastName)) {
              // if last name is present then add it
              query.append(" AND addr.lastName like '");
              query.append(StringUtils.replace(lastName, "*", "%") + "'");
            }
            isWhereClauseModified = true;
          }

          if (isIdentifactionPresent) {
            if (isWhereClauseModified) {
              query.append(" AND cust.id = iden.customer.id");
            }
            else {
              query.append(" cust.id = iden.customer.id");
            }

            if (StringUtils.hasText(accountNumber)) {
              query.append(" AND iden.dbActive='Y' AND iden.identificationType=8 AND iden.identifier like '");
              query.append(StringUtils.replace(accountNumber, "*", "%") + "'");
              query.append(" AND iden.orgUnit = '" + orgUnit + "'");
            }
            else if (StringUtils.hasText(msisdn)) {
              // guest handling search then get inactive msisdn as well
              query.append(" AND iden.dbActive = 'Y' AND iden.identificationType=0 AND iden.identifier like '");
              query.append(StringUtils.replace(msisdn, "*", "%") + "'");
              query.append(" AND iden.orgUnit = '" + orgUnit + "'");
            }
            isWhereClauseModified = true;
          }

          // If Agent id is present in search criteria
          if (StringUtils.hasText(agentID)) {
            if (isWhereClauseModified) {
              query.append(" AND lower(consDeatils.agentId) like '");
              query.append(StringUtils.replace(agentID.toLowerCase(), "*", "%") + "'");
              //  query.append(" AND consDeatils.customer.orgUnit.id = '" + orgUnit + "'");

            }
            else {
              query.append(" lower(consDeatils.agentId) like '");
              query.append(StringUtils.replace(agentID.toLowerCase(), "*", "%") + "'");
//	          query.append(" AND consDeatils.customer.orgUnit.id = '" + orgUnit + "'");
            }
            isWhereClauseModified = true;
          }

          // else search for active customers and agents
          query.append(isWhereClauseModified ?" AND":"");
          query.append(" cust.customerType.id in (");
          query.append(
                  searchCriteria.getCustomerType()!=null&&!searchCriteria.getCustomerType().isEmpty()?
                          searchCriteria.getCustomerType():"'60','61'");
          query.append(")");
          query.append(" AND cust.orgUnit.id = '" + orgUnit + "'");
          query.append(" AND cust.dbActive = 'Y'");
      /*
      if (StringUtils.hasText(registrationType) && !StringUtils.hasText(cnic)) {
        if (isWhereClauseModified) {
          query.append(" AND cust.strSpare8 in (");
          query.append(registrationType);
          query.append(")");
          isWhereClauseModified = true;
        }
        else {
          query.append(" cust.strSpare8 in (");
          query.append(registrationType);
          query.append(")");
          isWhereClauseModified = true;
        }
      }
      else {
        if (isWhereClauseModified) {
          query.append(" AND cust.customerType.id in ('52','60','61') ");

        }
        else {
          //query.append(" (cust.strSpare8 like '60' OR cust.strSpare8 like '61')");
          query.append(" cust.customerType.id in ('52','60','61') ");
        }
      }

      if(searchCriteria.getCustomerType()!= null && searchCriteria.getCustomerType().equals("52")) {
       // query.append(" AND cust.dbActive = 'Y' ");
        query.append(" AND cust.orgUnit = '" + orgUnit + "'");
      } else {
       // query.append(" AND cust.dbActive = 'Y'"); //AND cust.dbBolSpare5 != 'Y'AND cust.dbBolSpare5 != 'Y'
        query.append(" AND cust.orgUnit = '" + orgUnit + "'");
      }*/
          //PI
//	      if(searchCriteria.getRequestType().equalsIgnoreCase("linkaccount")){
//	    	  query.append(" AND cust.id = wallet.customer.id");
//	    	  query.append(" AND wallet.paymentInstrument.id = pi.id AND pi.type = 40 AND pi.dbActive = 'N'");
//	      }else if(searchCriteria.getRequestType().equalsIgnoreCase("delinkaccount")){
//	    	  query.append(" AND cust.id = pi.customer.id AND pi.type = 40 AND pi.dbActive = 'Y'");
//	          query.append(" AND pi.id = wallet.paymentInstrument.id");
//	      }


          LOG.info("==Customer Search HQL Query==" + query);

          List<Object[]> resultList = daoFactory.getUtilityDao().getResultByHQL(query.toString());

          customers = new ArrayList<CustomerSearchResultCBean>();

          for (Object[] objects : resultList) {
            if (objects.length >= 1) {

              Customer cust = (Customer) objects[0];
              String identityValue =  daoFactory.getUtilityDao().getIdentitiesByCustomerId(cust.getId()).get(0).getIdentity();
              CustomerSearchResultCBean customerSearchResult = searchResultConverter.tocontract(cust);
                customerSearchResult.setCnic(identityValue);

                if(searchCriteria.getCustomerType().equals(String.valueOf(CustomConstants.CUSTOMER_TYPE_L1))
                        || searchCriteria.getCustomerType().equals(String.valueOf(CustomConstants.CUSTOMER_TYPE_L2)))
                {
                    customerSearchResult.setBalance(customerUtil.getSVABalance(cust.getId()));
                }
              customers.add(customerSearchResult);
            }
          }

	    }
	    catch (Throwable t) {
	      LOG.error("Error Occurred While Searching Agents: " + t.getLocalizedMessage(), t);
	      throw new ExpressionException(11, t.getMessage());
	    }
	    return customers;

	  }

  @Override
  public void afterPropertiesSet() throws Exception {

  }

  /**
   * This method takes customerID and search all of its detail in system like address, SVA, type, PI etc
   * 
   */
  @Override
  public CustomerBean getCustomerDetailById(long customerId) throws ExpressionException {
    CustomerBean customerBean = null;
    Customer customer = null;
    com.sybase365.mobiliser.money.persistence.model.customer.Address address = null;
    com.sybase365.mobiliser.money.contract.v5_0.customer.beans.Customer customerV5 = null;
    com.sybase365.mobiliser.money.contract.v5_0.customer.beans.Address addressV5 = null;
    List<com.sybase365.mobiliser.money.persistence.model.customer.Identification> identificationList = null;
    List<com.sybase365.mobiliser.money.persistence.model.customer.Identity> identityList = null;
    List<CustomerAttribute> customerAttributeList = null;
    List<CustomerRole> customerRoleList = null;
    List<com.sybase365.mobiliser.money.persistence.model.customer.Attachment> attachmentList = null;
    ConsumerDetails consumerDetails = null;
    ConsumerDetailsCBean cd = null;
    List<PaymentInstrument> piList = null;
    List<com.sybase365.mobiliser.money.persistence.model.pi.WalletEntry> weList = null;
    CustomerInformation customerInformation=null;
    // AgentClassBean agentClassBean = null;
    String customerType = CustomConstants.SC_EMPTY;

    try {
      customer = moneyDaoFactory.getCustomerDao().getById(customerId);// daoFactory.getCustomCustomerDao().getById(customerId);

      if (customer == null) {
        throw new ExpressionException(CustomConstants.ERROR_MSG_CUSTOMER_NOT_FOUND);
      }
      else {
        customerBean = new CustomerBean();

        customerV5 = customerConverter.toContract(customer);

        customerBean.setCustomer(customerV5);

        customerType = customer.getStrSpare8();

        LOG.info("CustomerUtil.getCustomerDetailById.customerType: " + customerType);

        address = moneyDaoFactory.getAddressDao().getActiveCustomerAddressByType(customerId, CustomConstants.INT_ZERO);

        if (address != null) {
          addressV5 = customerConverter.toContract(address);

          customerBean.setCustomerAddress(addressV5);
        }

        identificationList = moneyDaoFactory.getIdentificationDao().getActiveByCustomerId(customerId);// daoFactory.getCustomCustomerDao().getIdentificationDetailsByCustomerId(customerId);

        if (identificationList != null && identificationList.size() > CustomConstants.INT_ZERO) {
          for (com.sybase365.mobiliser.money.persistence.model.customer.Identification identification : identificationList) {
            com.sybase365.mobiliser.money.contract.v5_0.customer.beans.Identification identificationV5 = customerConverter
                    .toContract(identification);

            customerBean.getCustomerIdentifications().add(identificationV5);
          }
        }

        //identities
        identityList = daoFactory.getUtilityDao().getIdentitiesByCustomerId(customerId);

        if (identityList != null && identityList.size() > CustomConstants.INT_ZERO) {
          for (com.sybase365.mobiliser.money.persistence.model.customer.Identity identity : identityList) {
            com.sybase365.mobiliser.money.contract.v5_0.customer.beans.Identity identity1 = customerConverter
                    .toContract(identity);

            customerBean.getCustomerIdentity().add(identity1);
          }
        }

        customerAttributeList = moneyDaoFactory.getCustomerAttributeDao().getAttributesByCustomerId(customerId);

        if (customerAttributeList != null && customerAttributeList.size() > CustomConstants.INT_ZERO) {
          for (CustomerAttribute attribute : customerAttributeList) {
            com.sybase365.mobiliser.money.contract.v5_0.customer.beans.CustomerAttribute attributeV5 = customerConverter
                    .toContract(attribute);

            customerBean.getCustomerAttributes().add(attributeV5);
          }
        }

        customerRoleList = moneyDaoFactory.getCustomerRoleDao().getRolesByCustomerId(customerId,
                customUtil.getCurrentDate());

        if (customerRoleList != null && customerRoleList.size() > CustomConstants.INT_ZERO) {
          if (customerRoleList.get(CustomConstants.INT_ZERO) != null) {
            com.sybase365.mobiliser.money.contract.v5_0.customer.beans.CustomerRole customerRoleV5 = umgrConverter
                    .toContract(customerRoleList.get(CustomConstants.INT_ZERO));

            customerBean.getCustomerRole().add(customerRoleV5);
          }
        }

        attachmentList = moneyDaoFactory.getAttachmentDao().getAttachmentsByCustomer(customerId, null);

        if (attachmentList != null && attachmentList.size() > CustomConstants.INT_ZERO) {
          for (com.sybase365.mobiliser.money.persistence.model.customer.Attachment attachment : attachmentList) {
            com.sybase365.mobiliser.money.contract.v5_0.customer.beans.Attachment attachmentV5 = customerConverter
                    .toContract(attachment);

            customerBean.getCustomerAttachment().add(attachmentV5);
          }
        }

        consumerDetails = daoFactory.getConsumerDetailsDao().getConsumerDetailsByCustomerId(customerId);

        if (consumerDetails != null) {
          cd = consumerConverter.toContract(consumerDetails);
          customerBean.setConsumerDetails(cd);

          // TODO Agent class name
          // if (consumerDetails.getAgentClassId() != null) {
          // AgentClass agentClass = daoFactory.getAgentClassDao().getById(consumerDetails.getAgentClassId());
          //
          // if (agentClass != null) {
          // //customerBean.setAgentClassName(agentClass.getAgentClassName());
          // agentClassBean = agentClassConverter.toContract(agentClass);
          //
          // customerBean.setAgentClassBean(agentClassBean);
          // }
          // }

          List<AgentPartner> apList = daoFactory.getAgentPartnerDao().getAgentPartnerDetailsByCustomerId(customerId);

          if (apList != null && apList.size() > CustomConstants.INT_ZERO) {
            for (AgentPartner agentPartner : apList) {
              AgentPartnerCBean ap = consumerConverter.toContract(agentPartner);

              customerBean.getAgentPartner().add(ap);
            }
          }

          List<AgentReceipts> arList = daoFactory.getAgentReceiptsDao().getAgentReceiptsByCustomerId(customerId);

          if (arList != null && arList.size() > CustomConstants.INT_ZERO) {
            for (AgentReceipts agentPartner : arList) {
              AgentReceiptsCBean ap = consumerConverter.toContract(agentPartner);

              customerBean.getAgentReceipts().add(ap);
            }
          }
        }

        piList = piLogic.getPaymentInstrumentsByCustomer(customerId, null, null, CustomConstants.FALSE);

        LOG.info("CustomerLogicImpl.piLIst: " + piList);

        if (piList != null && piList.size() > CustomConstants.INT_ZERO) {
          for (PaymentInstrument paymentInstrument : piList) {
            com.sybase365.mobiliser.money.contract.v5_0.wallet.beans.PaymentInstrument pi = piConverter
                    .toContract(paymentInstrument);

            if (pi != null) {
              customerBean.getCustomerPaymentInstrument().add(pi);
              if(pi.getType()==CustomConstants.PI_TYPE_BANK_ACCOUNT){
                BankAccount bankAccount = moneyDaoFactory.getBankAccountDao().getById(paymentInstrument.getId());
                com.sybase365.mobiliser.money.contract.v5_0.wallet.beans.BankAccount contractBankAccount=piConverter.toContract(bankAccount);
                customerBean.setCustomerBankAccount(contractBankAccount);
              }
            }
          }
        }

        weList = piLogic.getWalletEntriesByCustomer(customerId, null, null);

        LOG.info("CustomerLogicImpl.weList: " + weList);

        if (weList != null && weList.size() > CustomConstants.INT_ZERO) {
          for (com.sybase365.mobiliser.money.persistence.model.pi.WalletEntry walletEntry : weList) {
            com.sybase365.mobiliser.money.contract.v5_0.wallet.beans.WalletEntry we = piConverter
                    .toContract(walletEntry);

            customerBean.getCustomerWallet().add(we);
          }
        }

        customerInformation = daoFactory.getCustomerInformationDAO().getCustomerInformationByCustomerId(customerId);
        if (customerInformation !=null){
          CustomerInformationBean customerInformationBean =customerInformationConverter.toContract(customerInformation);
          customerBean.setCustomerInformation(customerInformationBean);
        }
      }
    }
    catch (ExpressionException e) {
      throw new ExpressionException("CustomerUtil.getCustomerDetailById", e);
    }
    catch (Exception e) {
      LOG.error("An error occurred in getCustomerDetailById() ", e);
      throw new ExpressionException(11, e.getMessage());
    }
    return customerBean;
  }

  public com.sybase365.mobiliser.money.persistence.dao.factory.api.DaoFactory getMoneyDaoFactory() {
    return moneyDaoFactory;
  }

  public void setMoneyDaoFactory(com.sybase365.mobiliser.money.persistence.dao.factory.api.DaoFactory moneyDaoFactory) {
    this.moneyDaoFactory = moneyDaoFactory;
  }

  public ICustomerConverter getCustomerConverter() {
    return customerConverter;
  }

  public IUmgrConverter getUmgrConverter() {
    return umgrConverter;
  }

  public void setUmgrConverter(IUmgrConverter umgrConverter) {
    this.umgrConverter = umgrConverter;
  }

  public IConsumerConverter getConsumerConverter() {
    return consumerConverter;
  }

  public void setConsumerConverter(IConsumerConverter consumerConverter) {
    this.consumerConverter = consumerConverter;
  }

  public IPIConverter getPiConverter() {
    return piConverter;
  }

  public void setPiConverter(IPIConverter piConverter) {
    this.piConverter = piConverter;
  }

  public void setCustomerConverter(ICustomerConverter customerConverter) {
    this.customerConverter = customerConverter;
  }

  public IPILogic getPiLogic() {
    return piLogic;
  }

  public void setPiLogic(IPILogic piLogic) {
    this.piLogic = piLogic;
  }

  public void setCustomerInformationConverter(ICustomerInformationConverter customerInformationConverter) {
    this.customerInformationConverter = customerInformationConverter;
  }
}
