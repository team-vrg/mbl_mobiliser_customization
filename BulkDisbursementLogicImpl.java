package com.sybase365.mobiliser.custom.project.businesslogic.impl;

import com.sybase365.mobiliser.custom.project.businesslogic.IBulkDisbursementLogic;
import com.sybase365.mobiliser.custom.project.businesslogic.IUtilityLogic;
import com.sybase365.mobiliser.custom.project.businesslogic.configuration.CustomTransactionUtil;
import com.sybase365.mobiliser.custom.project.businesslogic.configuration.CustomerUtil;
import com.sybase365.mobiliser.custom.project.businesslogic.configuration.ErrorConstants;
import com.sybase365.mobiliser.custom.project.businesslogic.configuration.PreferenceImplConstants;
import com.sybase365.mobiliser.custom.project.businesslogic.exceptions.BulkDisbursementException;
import com.sybase365.mobiliser.custom.project.businesslogic.exceptions.StatusCodes;
import com.sybase365.mobiliser.custom.project.businesslogic.impl.util.CustomConstants;
import com.sybase365.mobiliser.custom.project.businesslogic.impl.util.CustomUtil;
import com.sybase365.mobiliser.custom.project.converter.IBulkDisbursementSummaryConverter;
import com.sybase365.mobiliser.custom.project.converter.IBulkFileConverter;
import com.sybase365.mobiliser.custom.project.converter.impl.MakerCheckerConverter;
import com.sybase365.mobiliser.custom.project.jobs.event.model.BulkDisbursementEvent;
import com.sybase365.mobiliser.custom.project.persistence.bean.MakerChecker;
import com.sybase365.mobiliser.custom.project.persistence.bean.WorkItemBean;
import com.sybase365.mobiliser.custom.project.persistence.dao.factory.api.DaoFactory;
import com.sybase365.mobiliser.custom.project.persistence.model.BulkDisbursementSummary;
import com.sybase365.mobiliser.custom.project.persistence.model.BulkFile;
import com.sybase365.mobiliser.custom.project.persistence.model.WorkItem;
import com.sybase365.mobiliser.custom.project.services.contract.api.ICustomTransactionEndpoint;
import com.sybase365.mobiliser.custom.project.services.contract.v1_0.*;
import com.sybase365.mobiliser.custom.project.services.contract.v1_0.beans.*;
import com.sybase365.mobiliser.framework.contract.v5_0.base.MobiliserResponseType;
import com.sybase365.mobiliser.framework.event.generator.EventGenerator;
import com.sybase365.mobiliser.framework.service.api.MobiliserServiceException;
import com.sybase365.mobiliser.money.contract.v5_0.transaction.AuthorisationResponse;
import com.sybase365.mobiliser.money.contract.v5_0.transaction.beans.TransactionParticipant;
import com.sybase365.mobiliser.money.persistence.model.customer.Address;
import com.sybase365.mobiliser.money.persistence.model.customer.Customer;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.expression.ExpressionException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionOperations;

import javax.xml.bind.JAXBException;
import java.io.*;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Hassnan.ali
 * @description This class is responsible to handle business logic validation methods and Disbursement
 */
public class BulkDisbursementLogicImpl implements IBulkDisbursementLogic, InitializingBean {

    private static final Logger LOG = LoggerFactory.getLogger(BulkDisbursementLogicImpl.class);
    private DaoFactory daoFactory;
    private com.sybase365.mobiliser.money.persistence.dao.factory.api.DaoFactory moneyDaoFactory;

    private IUtilityLogic utilityLogic;
    private ICustomTransactionEndpoint transactionsService;
    private CustomTransactionUtil customTransactionUtil;
    private CustomUtil customUtil;
    private CustomerUtil customerUtil;
    //	private ICallerUtils callerUtils;
    private TransactionOperations transactionTemplate;
    //	private IMakerCheckerLogic makerCheckerLogic;
    //converters
    private IBulkDisbursementSummaryConverter bulkDisbursementSummaryConverter;
    private IBulkFileConverter bulkFileConverter;
    private MakerCheckerConverter makerCheckerConverter;
    private CustomLimitLogicImpl customLimitLogic;

    //mobiliser event
    protected EventGenerator eventGenerator;

    //class level variables to used for exception file creation
    //and maintaining records which needs to be sent to maker checker request
    private long endUserId;
    private List<String> fileErrorReportList;
    private long fileId;
    private BulkFileBean bulkFileContractBean;
    private long workItemId;

    @Override
    public void afterPropertiesSet() throws Exception {
        if (this.daoFactory == null) {
            throw new IllegalStateException("daoFactory is required");
        }

        if (this.moneyDaoFactory == null) {
            throw new IllegalStateException("moneyDaoFactory is required");
        }
    }

    @Override
    public BulkDisbursementMakerResponse createBulkDisbursementFileRequest(BulkDisbursementMakerRequest request, long callerId)
            throws BulkDisbursementException, IOException, ParseException {

        BulkDisbursementMakerResponse response = new BulkDisbursementMakerResponse();

        try {
            LOG.info("processBulkFile................: ");

            checkFile(request.getBulkUploadBean(), request.getOrgUnitId());
            createBulkFileAndSummary(request.getBulkUploadBean(), callerId, request.getOrgUnitId(), request.getMakerComments());

            MobiliserResponseType.Status status = new MobiliserResponseType.Status();
            status.setCode(CustomConstants.STATUS_CODE_SUCCESS);
            status.setValue("Application Id is " + workItemId);
            response.setStatus(status);

        } catch (BulkDisbursementException e) {
            response.setStatus(CustomUtil.createStatus(e));
            if (fileErrorReportList.size() > CustomConstants.INT_ZERO) {
                response.getStrErrorReportList().addAll(fileErrorReportList);
            }
        } catch (Exception e) {
            response.setStatus(CustomUtil.createDefaultStatus());
            LOG.error("ERROR: " + e);
        }

        return response;
    }

    @Override
    public SearchBulkDisbursementSummaryResponse fetchBulkDisbursementSummaryList(
            SearchBulkDisbursementSummaryRequest request) throws BulkDisbursementException {

        SearchBulkDisbursementSummaryResponse response = new SearchBulkDisbursementSummaryResponse();
        List<BulkDisbursementSummary> searchList = null;
        MobiliserResponseType.Status st = new MobiliserResponseType.Status();

        try {
            SearchBulkCriteriaBean searchCriteria = request.getSearchCriteria();
            if (searchCriteria.isIsLifeCycleMgtMakerSearchRequest()) {

                searchList = daoFactory.getBulkDisbursementSummaryDao().fetchBulkDisbSumListforLifeCycMgt(searchCriteria);
            } else if (searchCriteria.isIsLifeCycleMgtCheckerSearchRequest()) {

                searchList = new ArrayList<>();
                searchList.add(daoFactory.getBulkDisbursementSummaryDao().getById(searchCriteria.getSummaryId()));
            } else if (searchCriteria.isIsFileCheckerSearchDetailsRequest()) {

                searchList = daoFactory.getBulkDisbursementSummaryDao()
                        .fetchBulkDisbSummaryByFileIdFileStatusAndSummaryStatus(
                                searchCriteria.getFileId(), searchCriteria.getFileStatusId(),
                                searchCriteria.getSummaryStatusId1(), searchCriteria.getOrgUnitId());
            }

            if (searchList != null && searchList.size() > 0) {

                LOG.info("searchList.size(): " + searchList.size());

                for (BulkDisbursementSummary searchDisbSummryRec : searchList) {
                    BulkSummaryBean sdsr = bulkDisbursementSummaryConverter.toContract(searchDisbSummryRec);
                    response.getSearchBulkDisbursementSummary().add(sdsr);
                }

                LOG.info("response.getSearchBulkDisbursementSummary(): "
                        + response.getSearchBulkDisbursementSummary().size());
                st.setCode(CustomConstants.STATUS_CODE_SUCCESS);
                st.setValue(CustomConstants.STATUS_MSG_SUCCESS);
                response.setStatus(st);
            } else {
                if (searchCriteria.isIsLifeCycleMgtMakerSearchRequest()) {
                    throw new BulkDisbursementException(utilityLogic.fetchStatus(
                            StatusCodes.NO_DISB_SUMMARY_WITH_PARKED_STATUS, new Object[]{}));
                } else if (searchCriteria.isIsLifeCycleMgtCheckerSearchRequest()) {
                    throw new BulkDisbursementException(utilityLogic.fetchStatus(
                            StatusCodes.NO_DISB_SUMMARY_DOES_NOT_EXISTS, new Object[]{}));
                } else if (searchCriteria.isIsFileCheckerSearchDetailsRequest()) {
                    //CHECKER FILE
                    throw new BulkDisbursementException(utilityLogic.fetchStatus(
                            StatusCodes.NO_DISB_SUMMARY_WITH_INITIATED_STATUS, new Object[]{}));
                }
            }
        } catch (BulkDisbursementException e) {
            response.setStatus(CustomUtil.createStatus(e));
        }

        return response;
    }

    @Override
    public UpdateBulkDisbursementSummaryResponse updateBulkDisbursementSummary(
            UpdateBulkDisbursementSummaryRequest request, long callerId) throws BulkDisbursementException {

        UpdateBulkDisbursementSummaryResponse response = null;
        try {
            response = new UpdateBulkDisbursementSummaryResponse();
            BulkSummaryBean contractBean = request.getBulkDisbursementSummaryBean();
            BulkDisbursementSummary modelBean = null;

            //add common code here
            modelBean = bulkDisbursementSummaryConverter.toModel(contractBean);

            if (request.isIsMakerRequest()) {

                this.daoFactory.getBulkDisbursementSummaryDao().saveOrUpdate(modelBean, callerId);

                //As the Temporary records have been inserted now
                //Create the maker checker bean for request
                MakerCheckerBean makerCheckerBean = new MakerCheckerBean();
                makerCheckerBean.setOrgUnitId(modelBean.getOrgUnit().getId());
                makerCheckerBean.setWorkItemId(0l);
                makerCheckerBean.setStatusId(request.getActionStatusId());
                makerCheckerBean.setActionId(CustomConstants.WORK_ITEM_ACTION_CREATE);
                makerCheckerBean.setModuleId(CustomConstants.WORK_ITEM_MODULE_ID_BULK_DISBURSEMENT_SUMMARY);
                makerCheckerBean.setCreatedBy(callerId);
                makerCheckerBean.setCallerId(callerId);
                makerCheckerBean.setComments(request.getComments());
                makerCheckerBean.setSpareString1(modelBean.getMobile());
                makerCheckerBean.setSpareString2(modelBean.getCnic());
                makerCheckerBean.setSpareString5(modelBean.getBulkFile().getFileName());
                makerCheckerBean.setSpareString7(Long.toString(modelBean.getAmount()));
                makerCheckerBean.setSpareString8(Integer.toString(request.getCurrentStatus()));
                makerCheckerBean.setSpareString9(Integer.toString(request.getRequestedStatus()));
                makerCheckerBean.setSpareInteger1(modelBean.getBulkFile().getId());
                makerCheckerBean.setSpareInteger2(modelBean.getId());

                //			makerCheckerBean.setXmlData(contractBean);

                WorkItem workItem = new WorkItem();
                //			String xmlData = marshall(makerCheckerBean);
                WorkItemBean workItemBean = makerCheckerConverter.fromContractBean(makerCheckerBean, workItem, null);
                workItemId = saveWorkItem(workItemBean, callerId);

            } else if (request.isIsCheckerRequest()) {
                String statusId = request.getActionStatusId().toString();

                if (statusId.equals(CustomConstants.WORK_ITEM_STATUS_CODE_APPROVED)) {
                    if (request.getRequestedStatus() == CustomConstants.BULK_REC_STATUS_PARKED
                            || request.getRequestedStatus() == CustomConstants.BULK_REC_STATUS_INITIATED) {

                        if (modelBean.getBulkFile().getStatusId() == CustomConstants.BULK_FILE_STATUS_APPROVED) {
                            List<BulkDisbursementSummary> bulkDisbursementSummaryList = new ArrayList<>();
                            modelBean.setStatusId(CustomConstants.BULK_REC_STATUS_APPROVED);
                            bulkDisbursementSummaryList.add(modelBean);

                            processExistingDisbursement(bulkDisbursementSummaryList, callerId, modelBean.getOrgUnit().getId());
                        } else if (modelBean.getBulkFile().getStatusId() == CustomConstants.BULK_FILE_STATUS_UPLOAD) {
                            modelBean.setStatusId(CustomConstants.BULK_REC_STATUS_INITIATED);
                        } else if (modelBean.getBulkFile().getStatusId() == CustomConstants.BULK_FILE_STATUS_REJECT) {
                            modelBean.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
                        }
                    } else {
                        modelBean.setStatusId(request.getRequestedStatus());
                    }

                } else if (statusId.equals(CustomConstants.WORK_ITEM_STATUS_CODE_REJECTED)) {
                    if (request.getCurrentStatus() == CustomConstants.BULK_REC_STATUS_PARKED
                            || request.getCurrentStatus() == CustomConstants.BULK_REC_STATUS_INITIATED) {

                        if (modelBean.getBulkFile().getStatusId() == CustomConstants.BULK_FILE_STATUS_APPROVED) {
                            List<BulkDisbursementSummary> bulkDisbursementSummaryList = new ArrayList<>();
                            modelBean.setStatusId(CustomConstants.BULK_REC_STATUS_APPROVED);
                            bulkDisbursementSummaryList.add(modelBean);

                            processExistingDisbursement(bulkDisbursementSummaryList, callerId, modelBean.getOrgUnit().getId());
                        } else {
                            modelBean.setStatusId(request.getCurrentStatus());
                        }
                    } else {
                        //if request is rejected the old status should be placed
                        modelBean.setStatusId(request.getCurrentStatus());
                    }

                }

                this.daoFactory.getBulkDisbursementSummaryDao().saveOrUpdate(modelBean, callerId);
                daoFactory.getWorkItemDAO().updateRequest(request.getWorkItemId(), request.getComments(),
                        Integer.parseInt(statusId), null, callerId);
            }

            MobiliserResponseType.Status st = new MobiliserResponseType.Status();
            st.setCode(CustomConstants.STATUS_CODE_SUCCESS);
            st.setValue(CustomConstants.STATUS_MSG_SUCCESS);
            response.setStatus(st);
        } catch (BulkDisbursementException e) {
            response.setStatus(CustomUtil.createDefaultStatus());
            LOG.error("ERROR: " + e);
        }
        return response;
    }

    /**
     * @param callerId
     * @param fileId
     * @param orgUnitId
     * @param endUserId
     * @return
     * @throws BulkDisbursementException
     */
    @Override
    public void executeBulkDisbursement(long callerId, long fileId, String orgUnitId, long endUserId)
            throws BulkDisbursementException {

        List<BulkDisbursementSummary> bulkDisbursementSummaryList = daoFactory.getBulkDisbursementSummaryDao()
                .fetchBulkDisbSummaryWithFileIdAndStatusId(fileId, CustomConstants.BULK_REC_STATUS_INITIATED, orgUnitId);

        LOG.info("bulkDisbursementSummaryList size:" + bulkDisbursementSummaryList.size());

        executeDisbursement(bulkDisbursementSummaryList, fileId, callerId, orgUnitId, endUserId);

    }

    /**
     * this service takes the required parameters and executes the parked disbursement for customer L0
     *
     * @param cnic
     * @param mobile
     * @param callerId
     * @param orgUnitId
     * @throws BulkDisbursementException
     */
    @Override
    public void executeBulkDisbursementIfExists(String cnic, String mobile, long callerId, String orgUnitId)
            throws BulkDisbursementException {
        // fetch disbursement for the given cnic,mobile, orgUnitId
        // if disbursement exists --> process it --> else return
        // fetch endUser to do money transfer

        List<BulkDisbursementSummary> bulkDisbursementSummaryList = daoFactory.getBulkDisbursementSummaryDao()
                .fetchBulkDisbSummaryWithCnicMobileAndStatusId(cnic, mobile, CustomConstants.BULK_REC_STATUS_PARKED,
                        CustomConstants.BULK_FILE_STATUS_APPROVED, orgUnitId);

        if (bulkDisbursementSummaryList != null && bulkDisbursementSummaryList.size() > 0) {
            processExistingDisbursement(bulkDisbursementSummaryList, callerId, orgUnitId);
        } else {
            LOG.debug("No Disbursement Summary Found for the given cnic : " + cnic + " mobile : " + mobile);
        }
    }

    private void processExistingDisbursement(List<BulkDisbursementSummary> bulkDisbursementSummaryList, long callerId,
                                             String orgUnitId) throws BulkDisbursementException {

        long fileId = bulkDisbursementSummaryList.get(0).getBulkFile().getId();
        List<Integer> customerType = new ArrayList<Integer>();
        customerType.add(CustomConstants.CUSTOMER_TYPE_L2);

        Customer endUser = daoFactory.getCustomCustomerDao().fetchCustomerByCnicMobileAndType(
                bulkDisbursementSummaryList.get(0).getBulkFile().getCnic(),
                customUtil.getInternationalFormat(bulkDisbursementSummaryList.get(0).getBulkFile().getMobile()),
                bulkDisbursementSummaryList.get(0).getBulkFile().getOrgUnit().getId(), customerType, CustomConstants.IDENTITY_TYPE_NTN);

        if (endUser != null) {
            if (endUser.getBlacklistReason() == CustomConstants.BLACK_LIST_ACTIVE) {
                if (endUser.getBolSpare5() == null || !endUser.getBolSpare5()) {
                    endUserId = endUser.getId();
                    executeDisbursement(bulkDisbursementSummaryList, fileId, callerId, orgUnitId, endUserId);
                } else {
                    throw new BulkDisbursementException(utilityLogic.fetchStatus(
                            StatusCodes.ERROR_CUSTOMER_IS_SUSPENDED,
                            new Object[]{
                                    CustomConstants.STR_CUSTOMER_TYPE_CORPORATE,
                                    bulkDisbursementSummaryList.get(0).getBulkFile().getCnic(),
                                    customUtil.getInternationalFormat(bulkDisbursementSummaryList.get(0).getBulkFile()
                                            .getMobile())}));
                }
            } else {
                //9 means dormant account
                if (endUser.getBlacklistReason() == 9) {
                    throw new BulkDisbursementException(utilityLogic.fetchStatus(
                            StatusCodes.ERROR_TRANSACTION_PAYER_BLACKLISTED_DORMANT,
                            new Object[]{
                                    CustomConstants.STR_CUSTOMER_TYPE_CORPORATE,
                                    bulkDisbursementSummaryList.get(0).getBulkFile().getCnic(),
                                    customUtil.getInternationalFormat(bulkDisbursementSummaryList.get(0).getBulkFile()
                                            .getMobile())}));
                } else {
                    throw new BulkDisbursementException(utilityLogic.fetchStatus(
                            StatusCodes.ERROR_CUSTOMER_IS_BLACKLISTED,
                            new Object[]{
                                    CustomConstants.STR_CUSTOMER_TYPE_CORPORATE,
                                    bulkDisbursementSummaryList.get(0).getBulkFile().getCnic(),
                                    customUtil.getInternationalFormat(bulkDisbursementSummaryList.get(0).getBulkFile()
                                            .getMobile())}));
                }

            }
        } else {
            LOG.debug("Customer L2 not found or customer Inactive for given CNIC :"
                    + bulkDisbursementSummaryList.get(0).getBulkFile().getCnic() + " and Mobile : "
                    + customUtil.getInternationalFormat(bulkDisbursementSummaryList.get(0).getBulkFile().getMobile()));
            throw new BulkDisbursementException(utilityLogic.fetchStatus(
                    StatusCodes.ERROR_CUSTOMER_NOT_FOUND_WITH_CNIC_AND_MOBILE,
                    new Object[]{
                            CustomConstants.STR_CUSTOMER_TYPE_CORPORATE,
                            bulkDisbursementSummaryList.get(0).getBulkFile().getCnic(),
                            customUtil.getInternationalFormat(bulkDisbursementSummaryList.get(0).getBulkFile()
                                    .getMobile())}));

        }

    }

    @SuppressWarnings("unchecked")
    public void executeDisbursement(List<BulkDisbursementSummary> bulkDisbursementSummaryList, long fileId,
                                    long callerId, String orgUnitId, long endUserId) throws BulkDisbursementException {

        LOG.debug("BulkDisbursementLogicImpl.executeDisbursement called" + fileId);
        Integer parkedRecords = 0;

        // customerType for search L0
        final List<Integer> customerType = new ArrayList<Integer>();
        customerType.add(CustomConstants.CUSTOMER_TYPE_L1);

        for (BulkDisbursementSummary bulkDisbursementSummaryRes : bulkDisbursementSummaryList) {

            int parkRec = 0;
            String cnic = bulkDisbursementSummaryRes.getCnic();
            String mobile = customUtil.getInternationalFormat(bulkDisbursementSummaryRes.getMobile());
            long disbursementAmount = Long.valueOf(bulkDisbursementSummaryRes.getAmount());

            LOG.info("cnic: " + cnic);
            LOG.info("mobile: " + mobile);
            LOG.info("disbursementAmount: " + disbursementAmount);

            // Search Customer Based on Cnic and mobile
            // if exist --> Do Disbursement
            // else--> Marked Status Parked
            Customer customer = daoFactory.getCustomCustomerDao().fetchCustomerByCnicMobileAndType(cnic, mobile,
                    orgUnitId, customerType, CustomConstants.IDENTITY_TYPE_CNIC);

            Customer customerL2 = moneyDaoFactory.getCustomerDao().getById(endUserId);

            if (customer != null) {
                if (customer.getStrSpare10() != null && customerL2.getStrSpare10() != null && customer.getStrSpare10().equals(customerL2.getStrSpare10())) {
                    if (customer.getBlacklistReason() == CustomConstants.BLACK_LIST_ACTIVE) {
                        if (customer.getBolSpare5() == null || !customer.getBolSpare5()) {
                            if (disbursementAmount > CustomConstants.INT_ZERO) {

                                MakerChecker requestBean = new MakerChecker();
                                requestBean.setSpareString7(customer.getId().toString());
                                requestBean.setModuleId(CustomConstants.WORK_ITEM_MODULE_CODE_ACCOUNT_CLOSURE);
                                requestBean.setOrgUnitId(CustomConstants.ORG_UNIT);
                                requestBean.setStatusId(CustomConstants.WORK_ITEM_STATUS_CODE_PENDING);
                                List<MakerChecker> list = this.daoFactory.getWorkItemDAO().searchWorkItem(requestBean);

                                if (list == null) {
                                    DisbursementResponse response = null;
                                    DisbursementRequest disbursementRequest = new DisbursementRequest();

                                    disbursementRequest.setOrgUnitId(orgUnitId);
                                    // disbursementRequest.setAgentClassId(204);
                                    disbursementRequest.setAmount(new BigDecimal(bulkDisbursementSummaryRes.getAmount()));
                                    disbursementRequest.setTotalAmount(bulkDisbursementSummaryRes.getAmount());
                                    disbursementRequest.setHostBankId(orgUnitId);
                                    disbursementRequest.setEndUserId(endUserId);
                                    disbursementRequest.setPayeeId(customer.getId());
                                    disbursementRequest.setUseCaseId(CustomConstants.USE_CASE_BULK_DISBURSEMENT);

                                    try {
                                        response = transactionsService.executeDisbursement(disbursementRequest);
                                    } catch (BulkDisbursementException e) {
                                        bulkDisbursementSummaryRes.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
                                        bulkDisbursementSummaryRes.setComments(e.getMessage());
                                        //    long summaryId = bulkDisbursementSummaryRes.getId();
                                        //    BulkDisbursementSummary bulkDisbursementSummary = daoFactory.getBulkDisbursementSummaryDao().getById(summaryId);
                                        //    bulkDisbursementSummary.setComments(e.getMessage());
                                        //    bulkDisbursementSummary.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
                                        //    bulkDisbursementSummary.setLastUpdater(callerId);
                                        //    bulkDisbursementSummary.setLastUpdate(new Date());
                                        daoFactory.getBulkDisbursementSummaryDao().saveOrUpdate(bulkDisbursementSummaryRes, callerId);

                                    }
                                    if (response.getStatus().getCode() == CustomConstants.STATUS_CODE_SUCCESS
                                            && (response.getStatus().getValue() == null || response.getStatus().getValue()
                                            .equals(CustomConstants.STATUS_VALUE_OK))) {

                                        bulkDisbursementSummaryRes.setStatusId(CustomConstants.BULK_REC_STATUS_APPROVED);
                                        bulkDisbursementSummaryRes.setComments(new String("Disbursement Successful"));
                                        //    long summaryId = bulkDisbursementSummaryRes.getId();
                                        //   BulkDisbursementSummary bulkDisbursementSummary = daoFactory.getBulkDisbursementSummaryDao().getById(summaryId);
                                        LOG.info("resp.getStatus().getCode(): " + response.getStatus().getCode());
                                        // bulkDisbursementSummary.setStatusId(CustomConstants.BULK_REC_STATUS_APPROVED);
                                        //   bulkDisbursementSummary.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
                                        //   bulkDisbursementSummary.setComments(new String("Disbursement Successful"));
                                        //    bulkDisbursementSummary.setLastUpdater(callerId);
                                        //   bulkDisbursementSummary.setLastUpdate(new Date());
                                        daoFactory.getBulkDisbursementSummaryDao().saveOrUpdate(bulkDisbursementSummaryRes, callerId);

                                    } else {
                                        LOG.error(response.getStatus().getValue());
                                        bulkDisbursementSummaryRes.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
                                        bulkDisbursementSummaryRes.setComments(response.getStatus().getValue());
                                        //  long summaryId = bulkDisbursementSummaryRes.getId();
                                        //   BulkDisbursementSummary bulkDisbursementSummary = daoFactory.getBulkDisbursementSummaryDao().getById(summaryId);
                                        // bulkDisbursementSummary.setStatusId(CustomConstants.BULK_REC_STATUS_PENDING);
                                        //  bulkDisbursementSummary.setComments(response.getStatus().getValue());
                                        //   bulkDisbursementSummary.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
                                        //   bulkDisbursementSummary.setLastUpdater(callerId);
                                        //   bulkDisbursementSummary.setLastUpdate(new Date());
                                        daoFactory.getBulkDisbursementSummaryDao().saveOrUpdate(bulkDisbursementSummaryRes, callerId);
                                    }
                                } else {
                                    LOG.error("Cannot perform transaction. Account is pending to Close");
                                    bulkDisbursementSummaryRes.setComments("Cannot perform transaction. Customer account is pending to Close");
                                    bulkDisbursementSummaryRes.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
                                    daoFactory.getBulkDisbursementSummaryDao().saveOrUpdate(bulkDisbursementSummaryRes, callerId);
                                }
                            } else {
                                LOG.error("Amount should be greater than zero");
                                bulkDisbursementSummaryRes.setComments("Amount should be greater than zero");
                                bulkDisbursementSummaryRes.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
                                daoFactory.getBulkDisbursementSummaryDao().saveOrUpdate(bulkDisbursementSummaryRes, callerId);
                            }
                        } else {
                            LOG.error("Customer is suspended");
                            bulkDisbursementSummaryRes.setComments("Customer is suspended");
                            bulkDisbursementSummaryRes.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
                            daoFactory.getBulkDisbursementSummaryDao().saveOrUpdate(bulkDisbursementSummaryRes, callerId);
                        }
                    } else {

                        if (customer.getBlacklistReason() == 9) {
                            LOG.error("Customer is Dormant");
                            bulkDisbursementSummaryRes.setComments("Customer is Dormant");
                            bulkDisbursementSummaryRes.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
                            daoFactory.getBulkDisbursementSummaryDao().saveOrUpdate(bulkDisbursementSummaryRes, callerId);
                        } else {
                            LOG.error("Customer is Blacklisted");
                            bulkDisbursementSummaryRes.setComments("Customer is Blacklisted");
                            bulkDisbursementSummaryRes.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
                            daoFactory.getBulkDisbursementSummaryDao().saveOrUpdate(bulkDisbursementSummaryRes, callerId);
                        }
                    }
                } else {
                    LOG.error("Payroll Account must be associated with Corporate Account");
                    bulkDisbursementSummaryRes.setComments("Payroll Account must be associated with Corporate Account");
                    bulkDisbursementSummaryRes.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
                    daoFactory.getBulkDisbursementSummaryDao().saveOrUpdate(bulkDisbursementSummaryRes, callerId);
                }

            } else {
                parkRec++;
                LOG.error("Payroll Customer Not Found Or Customer Is Inactive");
                LOG.debug("Make Record As Parked.");
                bulkDisbursementSummaryRes.setComments("Payroll Customer Not Found Or Customer Is Inactive");
                bulkDisbursementSummaryRes.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
                daoFactory.getBulkDisbursementSummaryDao().saveOrUpdate(bulkDisbursementSummaryRes, callerId);

            }
        }
        /* need to remove this code but confirm because someone has update the event parkedRecords are not
        final Integer parkedRecord = parkedRecords;

		if (parkedRecord > 0) {
			BulkFile bulkFile = daoFactory.getBulkFileDao().getById(fileId);
			bulkFile.setStatusId(CustomConstants.BULK_FILE_STATUS_IN_PROGRESS);
			daoFactory.getBulkFileDao().saveOrUpdate(bulkFile, fileId);
		}*/

    }

    private void checkFile(BulkUploadBean requestBean, String orgUnitId) throws BulkDisbursementException, IOException {

        //creating arrayList for the file report
        fileErrorReportList = new ArrayList<>();
        BulkFile bulkFile = null;

        InputStream is;
        is = new ByteArrayInputStream(requestBean.getFileContent());
        BufferedReader reader;
        reader = new BufferedReader(new InputStreamReader(is));

        String line;

        int count = 0;
        long totalAmount = 0;
        long calculatedTotalAmount = 0;
        long endUserBalance = 0l;
        String companyName = null;

        while ((line = reader.readLine()) != null) {
            String[] strArray = line.split(",");

            if (strArray.length < 4) {
                /*throw new BulkDisbursementException(utilityLogic.fetchStatus(
                        StatusCodes.BULK_FILE_CONTAINS_WHITE_SPACE, new Object[] {}, count+1));*/
                fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                        StatusCodes.BULK_FILE_CONTAINS_WHITE_SPACE, new Object[]{}, count + 1));
            }

            if (count == 0) {
                if (strArray[0].equals("H")) {
                    if (strArray.length == 8) {
                        // ---check file duplication--------------------
                        bulkFile = new BulkFile();
                        bulkFile.setOrgUnit(moneyDaoFactory.getOrgUnitDao().getById(orgUnitId));
                        if (strArray[1].length() > CustomConstants.LENGTH_FILE_NAME_MIN
                                && strArray[1].length() <= CustomConstants.LENGTH_FILE_NAME_MAX
                                && strArray[1].matches(CustomConstants.REGEX_ALPHANUMERIC_NEW)) {
                            bulkFile.setFileName(strArray[1]);
                        } else {
                            /*throw new BulkDisbursementException(utilityLogic.fetchStatus(
                                    StatusCodes.BULK_FILE_NAME_SIZE, new Object[] {}, count+1));*/
                            fileErrorReportList.add((utilityLogic.fetchErrorWithLineNumber(
                                    StatusCodes.BULK_FILE_NAME_SIZE, new Object[]{}, count + 1)));
                        }

                        DateFormat formatter = new SimpleDateFormat(CustomConstants.DATE_FORMAT);

                        if (strArray[2].length() > CustomConstants.LENGTH_DATE_MIN) // check date length
                        {
                            Date date = null;
                            try {
                                date = formatter.parse(strArray[2]);
                                LOG.info("###date: " + date);
                                Date currentDate = new Date();

                                try {
                                    currentDate = formatter.parse(formatter.format(currentDate));
                                } catch (ParseException e) {
                                    LOG.info(e.getMessage());
                                }
                                LOG.info("Current date : " + currentDate.toString());

                                if (currentDate.compareTo(date) != 0) {
                                /*throw new BulkDisbursementException(utilityLogic.fetchStatus(
                                        StatusCodes.BULK_UPLOAD_CURRENT_DATE_MISS_MATCH_CODE, new Object[] {}, count+1));*/
                                    fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                                            StatusCodes.BULK_UPLOAD_CURRENT_DATE_MISS_MATCH_CODE, new Object[]{}, count + 1));

                                }

                                bulkFile.setFileDate(date);
                            } catch (ParseException e) {
                                fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                                        StatusCodes.BULK_INVALID_DATE_FORMAT_HEADER, new Object[]{}, count + 1));
                            }

                        } else {
                            fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                                    StatusCodes.BULK_DATE_NOT_FOUND_HEADER, new Object[]{}, count + 1));
                        }

                        LOG.info("CNIC : " + strArray[3]);

                        if (!strArray[3].matches(CustomConstants.REGEX_NTN)) {
                            fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                                    StatusCodes.HEADER_CNIC_NOT_VALID, new Object[]{}, count + 1));
                        }

                        if (!strArray[4].matches(CustomConstants.REGEX_MOBILE_NUMBER)) {
                            fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                                    StatusCodes.HEADER_MOBILE_NOT_VALID, new Object[]{}, count + 1));
                        }

                        // verify the L2 customer exists with the given CNIC,and, Mobile
                        List<Integer> customerType = new ArrayList<Integer>();
                        customerType.add(CustomConstants.CUSTOMER_TYPE_L2);

                        Customer customerL2 = daoFactory.getCustomCustomerDao().fetchCustomerByCnicMobileAndType(
                                strArray[3], customUtil.getInternationalFormat(strArray[4]), orgUnitId, customerType, CustomConstants.IDENTITY_TYPE_NTN);
                        if (customerL2 == null) {
                            fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                                    StatusCodes.ERROR_CUSTOMER_NOT_FOUND_WITH_CNIC_AND_MOBILE, new Object[]{
                                            CustomConstants.STR_CUSTOMER_TYPE_CORPORATE, strArray[3], strArray[4]}, count + 1));

                        } else if (customerL2.getBlacklistReason() == CustomConstants.BLACK_LIST_ACTIVE) {
                            if (customerL2.getBolSpare5() == null || !customerL2.getBolSpare5()) {
                                endUserId = customerL2.getId();
                                endUserBalance = customerUtil.getSVABalance(endUserId);
                            } else {
                                fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                                        StatusCodes.ERROR_CUSTOMER_IS_SUSPENDED, new Object[]{
                                                CustomConstants.STR_CUSTOMER_TYPE_CORPORATE, strArray[3], strArray[4]}, count + 1));
                            }
                        } else {
                            if (customerL2.getBlacklistReason() == 9) {
                                fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                                        StatusCodes.ERROR_CUSTOMER_IS_DORMANT, new Object[]{
                                                CustomConstants.STR_CUSTOMER_TYPE_CORPORATE, strArray[3], strArray[4]}, count + 1));
                            } else {
                                fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                                        StatusCodes.ERROR_CUSTOMER_IS_BLACKLISTED, new Object[]{
                                                CustomConstants.STR_CUSTOMER_TYPE_CORPORATE, strArray[3], strArray[4]}, count + 1));
                            }

                        }

                        companyName = customerL2 == null ? "" : customerL2.getStrSpare10();

                        if (Integer.parseInt(strArray[5]) > CustomConstants.LENGTH_COUNT_MIN) {
                            bulkFile.setCount(Integer.parseInt(strArray[5]));
                        } else {
                            fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                                    StatusCodes.BULK_TXN_COUNT_INVALID, new Object[]{}, count + 1));
                        }

                        if (!strArray[6].matches(CustomConstants.BULK_REGEX_AMOUNT)) {
                            fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                                    StatusCodes.HEADER_AMOUNT_NOT_VALID, new Object[]{}, count + 1));
                        }

                        long parsedAmount = customTransactionUtil.parseAmount(strArray[6],
                                CustomConstants.DEFAULT_CURRENCY_PKR);
                        LOG.info("parsedAmount: " + parsedAmount);

                        totalAmount = totalAmount + parsedAmount;

                        /*if (endUserBalance < totalAmount || endUserBalance <= CustomConstants.INT_ZERO) {
							*//*throw new BulkDisbursementException(utilityLogic.fetchStatus(
									StatusCodes.ERROR_SENDER_HAS_INSUFFICIENT_SVA_BALANCE, new Object[] {
											CustomConstants.STR_CUSTOMER_TYPE_L2, strArray[3], strArray[4] }));*//*

                            fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                                    StatusCodes.ERROR_SENDER_HAS_INSUFFICIENT_SVA_BALANCE, new Object[]{
                                            CustomConstants.STR_CUSTOMER_TYPE_L2, strArray[3], strArray[4]}, count + 1));
                        }*/

                        if (strArray[7].length() > CustomConstants.LENGTH_FILE_DISCRIPTION_MIN
                                && strArray[7].length() <= CustomConstants.LENGTH_FILE_DISCRIPTION_MAX
                                && strArray[7].matches(CustomConstants.REGEX_ALPHANUMERIC)) {
                            bulkFile.setFileDiscription(strArray[7]);
                        } else {
                            fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                                    StatusCodes.BULK_FILE_DISCRIPTION_SIZE, new Object[]{}, count + 1));
                        }

                        if (bulkFile.getFileName() != null) {
                            bulkFile.setFileName(bulkFile.getFileName().toUpperCase());
                            boolean isDuplicate = daoFactory.getBulkFileDao().isFileDuplicate(bulkFile);
                            LOG.info("isDuplicate:" + isDuplicate);

                            if (isDuplicate) {
                                fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(StatusCodes.DUPLICATE_FILE,
                                        new Object[]{}, count + 1));
                            }
                        }

                        if (customerL2 != null) {
                            LimitCheckBean limitCheckBean = customLimitLogic.limitCheck(CustomConstants.USE_CASE_BULK_DISBURSEMENT, customerL2.getId(), parsedAmount, false, 0l);
                            if (limitCheckBean.getErrorCode() != 0) {
                                if (ErrorConstants.ERROR_PAYER_SINGLE_LIMIT == limitCheckBean.getErrorCode()) {
                                    fileErrorReportList.add(ErrorConstants.ERROR_AT_LINE_NUMBER + (count + 1) + " ," + StatusCodes.ERROR_EXTERNAL_TRANSACTION_SINGLE_MINIMUM_UNDERRUN_MSG);
                                } else {
                                    fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(limitCheckBean.getErrorCode(), new Object[]{}, count + 1));
                                }
                            }
                        }


                    } else {
                        fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(StatusCodes.INCOMPLETE_INFO,
                                new Object[]{}, count + 1));
                    }
                } else {
                    fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(StatusCodes.HEADER_NOT_FOUND,
                            new Object[]{}, count + 1));
                    break;
                }
            } else if (count > 0) {
                if (strArray[0] == null || strArray[0].equals("")) {

                    if (strArray.length == 5) {

                        if (!strArray[1].matches(CustomConstants.REGEX_CNIC)
                                || strArray[1].length() < CustomConstants.LENGTH_CNIC
                                || strArray[1].length() > CustomConstants.LENGTH_CNIC) {
                            fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(StatusCodes.CNIC_NOT_VALID,
                                    new Object[]{}, count + 1));
                        }


                        if (!strArray[2].matches(CustomConstants.REGEX_MOBILE_NUMBER)) {
                            fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                                    StatusCodes.MOBILE_NUMBER_NOT_VALID, new Object[]{}, count + 1));

                        }

                        // verify the L1 customer exists with the given CNIC,and, Mobile
                        List<Integer> customerType = new ArrayList<Integer>();
                        customerType.add(CustomConstants.CUSTOMER_TYPE_L1);

                        Customer customerL1 = daoFactory.getCustomCustomerDao().fetchCustomerByCnicMobileAndType(
                                strArray[1], customUtil.getInternationalFormat(strArray[2]), orgUnitId, customerType, CustomConstants.IDENTITY_TYPE_CNIC);
                        if (customerL1 == null) {
                            fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                                    StatusCodes.ERROR_CUSTOMER_NOT_FOUND_WITH_CNIC_AND_MOBILE, new Object[]{
                                            CustomConstants.STR_CUSTOMER_TYPE_PAYROLL, strArray[1], strArray[2]}, count + 1));

                        } else if (customerL1.getBlacklistReason() == CustomConstants.BLACK_LIST_ACTIVE) {
                            if (customerL1.getBolSpare5() == null || !customerL1.getBolSpare5()) {
                                if (companyName != null && customerL1.getStrSpare10() != null && companyName.equals(customerL1.getStrSpare10())) {
                                    // do nothing
                                } else {
                                    fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                                            StatusCodes.ERROR_ACCOUNT_NOT_ASSOCIATED, new Object[]{}, count + 1));
                                }
                            } else {
                                fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                                        StatusCodes.ERROR_CUSTOMER_IS_SUSPENDED, new Object[]{
                                                CustomConstants.STR_CUSTOMER_TYPE_PAYROLL, strArray[1], strArray[2]}, count + 1));
                            }
                        } else {
                            //9 means dormant account
                            if (customerL1.getBlacklistReason() == 9) {
                                fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                                        StatusCodes.ERROR_CUSTOMER_IS_DORMANT, new Object[]{
                                                CustomConstants.STR_CUSTOMER_TYPE_PAYROLL, strArray[1], strArray[2]}, count + 1));
                            } else {
                                fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                                        StatusCodes.ERROR_CUSTOMER_IS_BLACKLISTED, new Object[]{
                                                CustomConstants.STR_CUSTOMER_TYPE_PAYROLL, strArray[1], strArray[2]}, count + 1));
                            }
                        }

                        // need to write rejex check for installment no
                       /* if (strArray[3].length() == CustomConstants.BULK_DISBURSEMENT_INSTALLMENT_LENGTH_MIN) {
							*//*throw new BulkDisbursementException(utilityLogic.fetchStatus(
									StatusCodes.BULK_DISBURSEMENT_INSTALLMENT_NO_REQUIRED, new Object[] {}, count+1));*//*
                            fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                                    StatusCodes.BULK_DISBURSEMENT_INSTALLMENT_NO_REQUIRED, new Object[]{}, count + 1));

                        }
                        if (strArray[3].length() > CustomConstants.BULK_DISBURSEMENT_INSTALLMENT_LENGTH_MAX) {
							*//*throw new BulkDisbursementException(utilityLogic.fetchStatus(
									StatusCodes.BULK_DISBURSEMENT_INSTALLMENT_LENGTH_INVALID, new Object[] {}, count+1));*//*
                            fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                                    StatusCodes.BULK_DISBURSEMENT_INSTALLMENT_LENGTH_INVALID, new Object[]{}, count + 1));
                        }*/
                        if (!strArray[3].matches(CustomConstants.BULK_REGEX_AMOUNT)) {
                            fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(StatusCodes.AMOUNT_NOT_VALID,
                                    new Object[]{}, count + 1));
                        }

                        long parsedAmount = CustomTransactionUtil.parseAmount(strArray[3],
                                CustomConstants.DEFAULT_CURRENCY_PKR);
                        LOG.info("parsedAmount: " + parsedAmount);

                        if (parsedAmount == 0) {
                            fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(StatusCodes.BULK_ZERO_AMOUNT,
                                    new Object[]{}, count + 1));
                        }

                        calculatedTotalAmount = calculatedTotalAmount + parsedAmount;

                        if (strArray[4].length() > CustomConstants.LENGTH_REC_DISCRIPTION_MIN
                                && strArray[4].length() <= CustomConstants.LENGTH_REC_DISCRIPTION_MAX
                                && strArray[4].matches(CustomConstants.REGEX_ALPHANUMERIC)) {

                            bulkFile.setFileDiscription(strArray[4]);
                        } else {
                            fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                                    StatusCodes.BULK_RECORD_DESCRIPTION_SIZE, new Object[]{}, count + 1));
                        }

                        /*boolean isFileProcessed = daoFactory.getBulkDisbursementSummaryDao().isRecordProcessed(strArray[1], strArray[2], strArray[3]);
                        if (isFileProcessed) {
                            fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(StatusCodes.ERROR_BULK_PROCESSED_RECORDS_FOUND,
                                    new Object[]{}, count + 1));
                        }*/

                        // Check customer limit

                        if (customerL1 != null) {
                            LimitCheckBean limitCheckBean = customLimitLogic.limitCheck(CustomConstants.USE_CASE_BULK_DISBURSEMENT, customerL1.getId(), parsedAmount, true, 0l);
                            if (limitCheckBean.getErrorCode() != 0) {
                                if (ErrorConstants.ERROR_PAYER_SINGLE_LIMIT == limitCheckBean.getErrorCode()) {
                                    fileErrorReportList.add(ErrorConstants.ERROR_AT_LINE_NUMBER + (count + 1) + " ," + StatusCodes.ERROR_EXTERNAL_TRANSACTION_SINGLE_MINIMUM_UNDERRUN_MSG);
                                } else {
                                    fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(limitCheckBean.getErrorCode(), new Object[]{}, count + 1));
                                }
                            }
                        }

                    } else {
                        fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(StatusCodes.INCOMPLETE_INFO,
                                new Object[]{}, count + 1));
                    }

                } else {
                    fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(StatusCodes.INVALID_INFO,
                            new Object[]{}, count + 1));
                }
            }
            count++;

        }

        if (fileErrorReportList.size() > CustomConstants.INT_ZERO) {
            throw new BulkDisbursementException(utilityLogic.fetchStatus(StatusCodes.ERROR_BULK_FILE_GENERAL_ERROR,
                    new Object[]{}));
        }

        if (totalAmount != calculatedTotalAmount) {
            fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(StatusCodes.TOTAL_AMOUNT_NOT_SAME,
                    new Object[]{}, 1));
        }

        if (totalAmount == 0) {
            fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(StatusCodes.BULK_ZERO_AMOUNT,
                    new Object[]{}, 1));
        }

        if (bulkFile != null && count - 1 != bulkFile.getCount()) {
            fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(StatusCodes.HEADER_TXNS_NOT_SAME,
                    new Object[]{}, 1));
        }

        if (bulkFile.getCount().intValue() > Integer.valueOf(PreferenceImplConstants.getBulkFileCount()).intValue()) {
            fileErrorReportList.add(utilityLogic.fetchErrorWithLineNumber(
                    StatusCodes.BULK_TOTAL_COUNT_LIMIT,
                    new Object[]{PreferenceImplConstants.getBulkFileCount()}, count + 1));
        }

        if (fileErrorReportList.size() > CustomConstants.INT_ZERO) {
            throw new BulkDisbursementException(utilityLogic.fetchStatus(StatusCodes.ERROR_BULK_FILE_GENERAL_ERROR,
                    new Object[]{}));
        }
        requestBean.setTotalTxns(count);
    }

    private void createBulkFileAndSummary(final BulkUploadBean requestBean, final long callerId, final String orgUnitId, final String makerComments)
            throws BulkDisbursementException, IOException, ParseException {
        try {
            this.transactionTemplate.execute(new TransactionCallback() {
                @Override
                public MobiliserResponseType.Status doInTransaction(TransactionStatus arg0) {
                    MobiliserResponseType.Status status = new MobiliserResponseType.Status();

                    LOG.info("#createBulkFileAndSummary");

                    fileId = 0l;
                    String line;
                    boolean response = false;
                    int totalTxns = 0;
                    BulkFile bulkFileMBean = null;

                    byte[] file = requestBean.getFileContent();
                    InputStream is = new ByteArrayInputStream(file);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    try {
                        while ((line = reader.readLine()) != null) {

                            if (totalTxns == requestBean.getTotalTxns()) {
                                LOG.info("1: " + totalTxns);
                                LOG.info("2: " + requestBean.getTotalTxns());
                                break;
                            }
                            String[] strArray = line.split(",");
                            BulkDisbursementSummary bulkDisbursementSummary = new BulkDisbursementSummary();

                            if (!(strArray[0].equals("H"))) {

                                bulkDisbursementSummary.setCnic(strArray[1].trim());
                                bulkDisbursementSummary.setMobile(strArray[2].trim());
                                // bulkDisbursementSummary.setInstallmentNo(strArray[3].trim());
                                bulkDisbursementSummary.setStatusId(CustomConstants.BULK_REC_STATUS_INITIATED);
                                bulkDisbursementSummary.setAmount(customTransactionUtil.parseAmount(strArray[3], CustomConstants.DEFAULT_CURRENCY_PKR));
                                bulkDisbursementSummary.setDescription(strArray[4]);
                                bulkDisbursementSummary.setBulkFile(bulkFileMBean);
                                bulkDisbursementSummary.setCreator(callerId);
                                bulkDisbursementSummary.setCreationDate(new Date());
                                bulkDisbursementSummary.setOrgUnit(moneyDaoFactory.getOrgUnitDao().getById(orgUnitId));

                                daoFactory.getBulkDisbursementSummaryDao().save(bulkDisbursementSummary, callerId);
                                response = true;
                            } else {

                                bulkFileMBean = new BulkFile();
                                bulkFileMBean.setFileName((strArray[1].trim()).toUpperCase());
                                DateFormat formatter = new SimpleDateFormat(CustomConstants.DATE_FORMAT);
                                Date date = formatter.parse(strArray[2]);
                                LOG.info("###date: " + date);
                                bulkFileMBean.setFileDate(date);
                                bulkFileMBean.setCnic(strArray[3]);
                                bulkFileMBean.setMobile(strArray[4]);
                                bulkFileMBean.setCount(Integer.parseInt(strArray[5]));
                                bulkFileMBean.setAmount(customTransactionUtil.parseAmount(strArray[6], CustomConstants.DEFAULT_CURRENCY_PKR));
                                bulkFileMBean.setFileDiscription(strArray[7].trim());
                                bulkFileMBean.setCreator(callerId);
                                bulkFileMBean.setStatusId(CustomConstants.BULK_FILE_STATUS_UPLOAD);
                                bulkFileMBean.setFileTypeId(CustomConstants.BULK_FILE_TYPE_DISBURSEMENT);
                                bulkFileMBean.setOrgUnit(moneyDaoFactory.getOrgUnitDao().getById(orgUnitId));
                                daoFactory.getBulkFileDao().save(bulkFileMBean, callerId);

                                fileId = bulkFileMBean.getId();
                                if (fileId == 0) {
                                    throw new BulkDisbursementException(utilityLogic.fetchStatus(
                                            StatusCodes.BULK_FILE_NOT_SAVED, new Object[]{}));
                                }
                                bulkFileContractBean = bulkFileConverter.toContract(bulkFileMBean);
                            }

                            totalTxns++;
                        }

                        //TODO remove this if because now event runs on approval
                        if (response) {
                            LOG.info("###agent.getId(): " + callerId);

                            BulkDisbursementEvent bulkDisbursementEvent = new BulkDisbursementEvent();
                            bulkDisbursementEvent.setCallerId(callerId);
                            bulkDisbursementEvent.setFileId(bulkFileMBean.getId());
                            bulkDisbursementEvent.setOrgUnitId(orgUnitId);
                            bulkDisbursementEvent.setEndUserId(endUserId);
//							eventGenerator.create(bulkDisbursementEvent);
                        }

                        //As the Temporary records have been inserted now
                        //Create the maker checker bean for request
                        MakerCheckerBean makerCheckerBean = new MakerCheckerBean();
                        makerCheckerBean.setOrgUnitId(orgUnitId);
                        makerCheckerBean.setWorkItemId(0l);
                        makerCheckerBean.setStatusId(CustomConstants.ID_STATUS_PENDING);
                        makerCheckerBean.setActionId(CustomConstants.WORK_ITEM_ACTION_CREATE);
                        makerCheckerBean.setModuleId(CustomConstants.WORK_ITEM_MODULE_ID_BULK_DISBURSEMENT);
                        makerCheckerBean.setCreatedBy(requestBean.getMakerId());
                        makerCheckerBean.setCallerId(callerId);
                        makerCheckerBean.setComments(makerComments);
                        makerCheckerBean.setSpareString1(bulkFileMBean.getMobile());
                        makerCheckerBean.setSpareString2(bulkFileMBean.getCnic());
                        makerCheckerBean.setSpareString5(bulkFileMBean.getFileName());
                        makerCheckerBean.setSpareString6(Integer.toString(bulkFileMBean.getCount()));
                        makerCheckerBean.setSpareString7(Long.toString(bulkFileMBean.getAmount()));
                        makerCheckerBean.setSpareInteger1(fileId);
                        makerCheckerBean.setXmlData(bulkFileContractBean);

//						workItemId = makerCheckerLogic.createRequest(makerCheckerBean, callerId).getWorkItemId();
                        WorkItem workItem = new WorkItem();
                        String xmlData = marshall(makerCheckerBean);
                        WorkItemBean workItemBean = makerCheckerConverter.fromContractBean(makerCheckerBean, workItem, xmlData);
                        workItemId = saveWorkItem(workItemBean, callerId);
                    } catch (ParseException pe) {
                        LOG.error("Error : " + pe);
                    } catch (IOException e) {
                        LOG.error("Error : " + e);
                    }
                    return status;
                }
            });
        } catch (DataIntegrityViolationException e) {
            throw new BulkDisbursementException(utilityLogic.fetchStatus(
                    StatusCodes.ERROR_BULK_PROCESSED_RECORDS_FOUND, new Object[]{}));
        }
    }

    private String marshall(MakerCheckerBean makerCheckerBean) {
        String xmlData = null;
        try {
            xmlData = customUtil.marshal(makerCheckerBean.getXmlData(), MakerCheckerBean.class);
        } catch (JAXBException jaxbEx) {
            LOG.error("Marshall Exception:" + jaxbEx);
        }
        return xmlData;
    }

    private long saveWorkItem(WorkItemBean workItemBean, long callerId) {
        long workItemId = daoFactory.getWorkItemDAO().saveWorkItem(workItemBean, callerId);
        if (workItemId > 0) {
            daoFactory.getWorkItemDAO().saveWorkItemXml(workItemBean.getWorkItemXml(), workItemId, callerId);
            if (workItemBean.getWorkItemComment().getRevisionComments() != null) {
                workItemBean.getWorkItemComment().getId().setWorkItemId(workItemId);
                daoFactory.getWorkItemDAO().saveWorkItemComment(workItemBean.getWorkItemComment(), callerId);
            }
        }
        return workItemId;
    }

    public BulkDisbursementCheckerResponse updateBulkDisbursementFileRequest(BulkDisbursementCheckerRequest request, long callerId) {

        BulkDisbursementCheckerResponse response = new BulkDisbursementCheckerResponse();
        MobiliserResponseType.Status status = new MobiliserResponseType.Status();

        MakerCheckerBean makerCheckerBean = request.getMakerCheckerBean();
        //fetch the file by using id
        long fileId = makerCheckerBean.getSpareInteger1();
        BulkFile bulkFile = daoFactory.getBulkFileDao().getById(fileId);

        if (makerCheckerBean.getStatusId().toString().equals(CustomConstants.WORK_ITEM_STATUS_CODE_REJECTED)) {
            try {
                daoFactory.getBulkDisbursementSummaryDao().updateBulkDisbursementFileSummary(bulkFile.getId(), CustomConstants.BULK_REC_STATUS_REJECT, bulkFile.getOrgUnit().getId());
            } catch (HibernateException he) {
                throw new BulkDisbursementException(utilityLogic.fetchStatus(
                        StatusCodes.ERROR_BULK_FILE_SUMMARY_REJECT_ERROR, new Object[]{}));
            }
            bulkFile.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
            daoFactory.getBulkFileDao().update(bulkFile, callerId);

            //if the file is rejected then reject all the pending Life cycle Mgt. request associate the that file
            String query = new StringBuilder().append("update ABS_WORK_ITEMS awi set ID_STATUS = ")
                    .append(CustomConstants.WORK_ITEM_STATUS_CODE_REJECTED + " where INT_SPARE_1 = ")
                    .append(fileId + " and INT_SOURCE_MODULE = " + CustomConstants.WORK_ITEM_MODULE_ID_BULK_DISBURSEMENT_SUMMARY)
                    .append(" and ID_STATUS = " + CustomConstants.WORK_ITEM_STATUS_CODE_PENDING).toString();
            daoFactory.getUtilityDao().executeSQLQuery(query);

            // send rejection sms
            WorkItem workItem = daoFactory.getWorkItemDAO().getById(makerCheckerBean.getWorkItemId());
            Long makerId = workItem.getCreator();
            Customer customer = moneyDaoFactory.getCustomerDao().getById(makerId);
            List<Address> address = moneyDaoFactory.getAddressDao().getCustomerAddresses(makerId, true);
            if (address != null && address.get(0).getEmail() != null) {
                LOG.info("Sending email to: " + makerId + " and email address: " + address.get(0).getEmail());
                CustomUtil.sendRejectEmailNotification(address.get(0).getEmail(), customer.getDisplayName(), makerCheckerBean.getWorkItemId(), makerCheckerBean.getComments(), makerCheckerBean.getCallerId(), makerCheckerBean.getSpareString5(), CustomConstants.MSG_TEMPLATE_BULK_DISBURSEMENT_REJECT_REQUEST);
            }
        } else {
            //defining try catch here because when we try to create status
            //at endpoint mobiliser does not allow it and create the error 9935-DB
            try {
                //mark file as approved
                List<Integer> customerType = new ArrayList<Integer>();
                customerType.add(CustomConstants.CUSTOMER_TYPE_L2);
                Customer customerL2 = daoFactory.getCustomCustomerDao().fetchCustomerByCnicMobileAndType(
                        bulkFile.getCnic(), customUtil.getInternationalFormat(bulkFile.getMobile()), bulkFile.getOrgUnit().getId(), customerType, CustomConstants.IDENTITY_TYPE_NTN);
                if (customerL2 != null) {

                    long totalAmount = bulkFile.getAmount();
                    LOG.info("file total amount : " + totalAmount);

                    endUserId = customerL2.getId();
                    long endUserBalance = 0l;

                    if (customerL2.getBlacklistReason() == CustomConstants.BLACK_LIST_ACTIVE) {
                        if (customerL2.getBolSpare5() == null || !customerL2.getBolSpare5()) {

                            // Check customers limit
                            long transferInAmount = checkPayrollCustomersLimit(fileId, makerCheckerBean.getOrgUnitId(), endUserId, callerId);

                            if (transferInAmount > CustomConstants.INT_ZERO) {

                                MobiliserResponseType.Status transactionStatus = TransferInTransactionForBulk(customerL2, transferInAmount, callerId);

                                if (transactionStatus.getCode() == 0) {
                                    endUserId = customerL2.getId();
                                    endUserBalance = customerUtil.getSVABalance(endUserId);
                                } else {
                                    throw new BulkDisbursementException(transactionStatus);
                                }

                            } else {
                                // throw exception and reject the file
                                /*bulkFile.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
                                daoFactory.getBulkFileDao().update(bulkFile, callerId);
                                */
                                throw new BulkDisbursementException(utilityLogic.fetchStatus(
                                        StatusCodes.BULK_FILE_PROCESSING_FAILED, new Object[]{}));
                            }

                        } else {
                            throw new BulkDisbursementException(utilityLogic.fetchStatus(
                                    StatusCodes.ERROR_CUSTOMER_IS_SUSPENDED, new Object[]{
                                            CustomConstants.STR_CUSTOMER_TYPE_CORPORATE, bulkFile.getCnic(), bulkFile.getMobile()}));
                        }
                    } else {
                        //9 means dormant account
                        if (customerL2.getBlacklistReason() == 9) {
                            throw new BulkDisbursementException(utilityLogic.fetchStatus(
                                    StatusCodes.ERROR_TRANSACTION_PAYER_BLACKLISTED_DORMANT, new Object[]{
                                            CustomConstants.STR_CUSTOMER_TYPE_CORPORATE, bulkFile.getCnic(), bulkFile.getMobile()}));
                        } else {
                            throw new BulkDisbursementException(utilityLogic.fetchStatus(
                                    StatusCodes.ERROR_CUSTOMER_IS_BLACKLISTED, new Object[]{
                                            CustomConstants.STR_CUSTOMER_TYPE_CORPORATE, bulkFile.getCnic(), bulkFile.getMobile()}));
                        }
                    }

                    //check the L2 balance before approving file
                    //if (endUserBalance < totalAmount || endUserBalance <= CustomConstants.INT_ZERO) {
                    if (endUserBalance <= CustomConstants.INT_ZERO) {
                        throw new BulkDisbursementException(utilityLogic.fetchStatus(
                                StatusCodes.ERROR_SENDER_HAS_INSUFFICIENT_SVA_BALANCE, new Object[]{
                                        CustomConstants.STR_CUSTOMER_TYPE_CORPORATE, bulkFile.getCnic(), bulkFile.getMobile()}));
                    }

                    BulkDisbursementEvent bulkDisbursementEvent = new BulkDisbursementEvent();
                    bulkDisbursementEvent.setCallerId(callerId);
                    bulkDisbursementEvent.setFileId(bulkFile.getId());
                    bulkDisbursementEvent.setOrgUnitId(bulkFile.getOrgUnit().getId());
                    bulkDisbursementEvent.setEndUserId(endUserId);
                    //then run the event
                    eventGenerator.create(bulkDisbursementEvent);
                } else {
                    throw new BulkDisbursementException(utilityLogic.fetchStatus(
                            StatusCodes.ERROR_CUSTOMER_NOT_FOUND_WITH_CNIC_AND_MOBILE, new Object[]{
                                    CustomConstants.STR_CUSTOMER_TYPE_CORPORATE, bulkFile.getCnic(), bulkFile.getMobile()}));
                }
                bulkFile.setStatusId(CustomConstants.BULK_REC_STATUS_APPROVED);
                daoFactory.getBulkFileDao().update(bulkFile, callerId);

            } catch (BulkDisbursementException e) {
                response.setStatus(CustomUtil.createStatus(e));
            }
        }

        //update workItem entry if no exception has occurred
        if (response.getStatus() == null)
            response.setStatus(

                    updateWorkItemRequest(makerCheckerBean)

            );
        return response;
    }

    public MobiliserResponseType.Status TransferInTransactionForBulk(Customer customer, Long amount, final Long callerId) {


        MobiliserResponseType.Status status = new MobiliserResponseType.Status();

        try {
            customerUtil.validateCustomer(customer);

            final String formattedAmount = customTransactionUtil.formatAmount(BigDecimal.valueOf(amount), CustomConstants.CURRENCY_CODE);
            LOG.info("Formatted Amount: " + formattedAmount);

            final TransactionParticipant payerPart = utilityLogic.prepareActor(customer, CustomConstants.IDENTIFICATION_TYPE_CUSTOMER_ID,
                    CustomConstants.PI_CLASS_BANK, CustomConstants.PI_TYPE_BANK);

            if (payerPart == null) {
                throw new BulkDisbursementException(utilityLogic.fetchStatus(StatusCodes.ERROR_TRANSFER_IN_NO_BANK_ACCOUNT_LINKED, new Object[]{}));
            }

            final TransactionParticipant payeePart = utilityLogic.prepareActor(customer, CustomConstants.IDENTIFICATION_TYPE_CUSTOMER_ID,
                    CustomConstants.PI_CLASS_SVA, CustomConstants.PI_TYPE_SVA);

            AuthorisationResponse authResponse = (AuthorisationResponse) this.transactionTemplate.execute(new TransactionCallback() {
                @Override
                public AuthorisationResponse doInTransaction(TransactionStatus arg0) {
                    AuthorisationResponse authResponse = customTransactionUtil.doTransaction(payerPart, payeePart, formattedAmount, CustomConstants.USE_CASE_TRANSFER_IN,
                            CustomConstants.TRANSFER_IN_FOR_BULK, CustomConstants.ID_ORDER_CHANNEL_PORTAL, 0l, callerId);
                    return authResponse;
                }

            });

            if (authResponse != null) {
                if (authResponse.getTransaction().getSystemId() > 0) {
                    status.setCode(CustomConstants.STATUS_CODE_OK);
                    status.setValue(CustomConstants.STATUS_VALUE_OK);
                } else {
                    status.setCode(authResponse.getStatus().getCode());
                    status.setValue(authResponse.getStatus().getValue());
                }
            } else {
                status.setCode(StatusCodes.ERROR_TRANSFER_IN_TRANSACTION_FAILED);
                status.setValue("Transfer In Failed");
            }

        } catch (BulkDisbursementException e) {
            LOG.info("Original Error Code: " + e.getCode());
            LOG.info("Original Error Message: " + utilityLogic.fetchStatus(e.getCode(), new Object[]{}).getValue());
            status.setCode(e.getCode());
            status.setValue(utilityLogic.fetchStatus(e.getCode(), new Object[]{}).getValue());
        } catch (MobiliserServiceException e) {
            LOG.info("Original Error Code: " + e.getErrorCode());
            LOG.info("Original Error Message: " + utilityLogic.fetchStatus(e.getErrorCode(), new Object[]{}).getValue());
            if (e.getErrorCode() == 2553
                    || e.getErrorCode() == 2554
                    || e.getErrorCode() == 2562
                    || e.getErrorCode() == 2563
                    || e.getErrorCode() == 2564
                    || e.getErrorCode() == 2565
                    || e.getErrorCode() == 2566
                    || e.getErrorCode() == 2567
                    || e.getErrorCode() == 2568
                    || e.getErrorCode() == 2569
                    || e.getErrorCode() == 2573
                    || e.getErrorCode() == 2574
                    || e.getErrorCode() == 2594
                    || e.getErrorCode() == 2595
                    || e.getErrorCode() == 2596
                    || e.getErrorCode() == 2597) {
                status.setCode(StatusCodes.ERROR_EXTERNAL_TRANSACTION_LIMITS_EXCEED);
                status.setValue(StatusCodes.ERROR_EXTERNAL_TRANSACTION_LIMITS_EXCEED_MSG);
            } else if (e.getErrorCode() == 2593
                    || e.getErrorCode() == 2592
                    || e.getErrorCode() == 2570) {
                status.setCode(StatusCodes.ERROR_EXTERNAL_TRANSACTION_SINGLE_MINIMUM_UNDERRUN);
                status.setValue(StatusCodes.ERROR_EXTERNAL_TRANSACTION_SINGLE_MINIMUM_UNDERRUN_MSG);
            } else {
                status.setCode(e.getErrorCode());
                status.setValue(utilityLogic.fetchStatus(e.getErrorCode(), new Object[]{}).getValue());
            }
            return status;
        } catch (ExpressionException e) {
            LOG.info("Original Error Code: " + e.getPosition());
            LOG.info("Original Error Message: " + e.toDetailedString());
            status.setCode(e.getPosition());
            status.setValue(e.toDetailedString());
            return status;
        } catch (Exception e) {
            status.setCode(StatusCodes.ERROR_TRANSFER_IN_TXN_FAILED);
            status.setValue(utilityLogic.fetchStatus(StatusCodes.ERROR_TRANSFER_IN_TXN_FAILED, new Object[]{}).getValue());
            return status;
        }
        return status;
    }

/*    private void validateCustomer(Customer payer) {
        if (payer == null) {
            throw new BulkDisbursementException(utilityLogic.fetchStatus(StatusCodes.ERROR_TRANSACTION_CUSTOMER_NOT_FOUND, new Object[]{}));
        }

        if (payer.getBlacklistReason() != 0) {
            throw new BulkDisbursementException(utilityLogic.fetchStatus(StatusCodes.ERROR_TRANSACTION_PAYER_BLACKLISTED, new Object[]{}));
        }

        if (!utilityLogic.isCustomer(payer)) {
            throw new BulkDisbursementException(utilityLogic.fetchStatus(StatusCodes.ERROR_TRANSACTION_PAYER_MUST_BE_CUSTOMER, new Object[]{payer.getId()}));
        }

        if (utilityLogic.isSuspended(payer.getOrgUnit().getId(), payer.getId())) {
            throw new BulkDisbursementException(utilityLogic.fetchStatus(StatusCodes.ERROR_TRANSACTION_PAYER_SUSPENDED, new Object[]{payer.getId()}));
        }

        if (!utilityLogic.isAgentClassActiveForAgent(payer.getFeeSetId())) {
            throw new CustomerIBFTException(utilityLogic.fetchStatus(StatusCodes.ERROR_TRANSACTION_AGENT_CLASS_IN_ACTIVE,
                    new Object[]{payer.getFeeSetId()}));
        }
    }*/

    private MobiliserResponseType.Status updateWorkItemRequest(MakerCheckerBean makerCheckerBean) {
        MobiliserResponseType.Status status = new MobiliserResponseType.Status();
        String xmlData = null;
        try {
            xmlData = customUtil.marshal(makerCheckerBean.getXmlData(), MakerCheckerBean.class);
        } catch (JAXBException jaxb) {
            LOG.error("Error Occured during marshal: " + jaxb);
        }
        daoFactory.getWorkItemDAO().updateRequest(makerCheckerBean.getWorkItemId(), makerCheckerBean.getComments(),
                makerCheckerBean.getStatusId(), xmlData, makerCheckerBean.getCallerId());
        status.setCode(CustomConstants.STATUS_CODE_SUCCESS);
        status.setValue(CustomConstants.STATUS_MSG_SUCCESS);
        return status;
    }


    public long checkPayrollCustomersLimit(long fileId, String orgUnitId, long endUserId, long callerId) {

        long amountForTransferIn = 0l;

        List<BulkDisbursementSummary> bulkDisbursementSummaryList = daoFactory.getBulkDisbursementSummaryDao()
                .fetchBulkDisbSummaryWithFileIdAndStatusId(fileId, CustomConstants.BULK_REC_STATUS_INITIATED, orgUnitId);

        LOG.debug("BulkDisbursementLogicImpl.executeDisbursement called" + fileId);
        Integer parkedRecords = 0;

        // customerType for search L0
        final List<Integer> customerType = new ArrayList<Integer>();
        customerType.add(CustomConstants.CUSTOMER_TYPE_L1);

        Customer customerL2 = moneyDaoFactory.getCustomerDao().getById(endUserId);

        for (BulkDisbursementSummary bulkDisbursementSummaryRes : bulkDisbursementSummaryList) {

            int parkRec = 0;
            String cnic = bulkDisbursementSummaryRes.getCnic();
            String mobile = customUtil.getInternationalFormat(bulkDisbursementSummaryRes.getMobile());
            long disbursementAmount = Long.valueOf(bulkDisbursementSummaryRes.getAmount());

            LOG.info("cnic: " + cnic);
            LOG.info("mobile: " + mobile);
            LOG.info("disbursementAmount: " + disbursementAmount);

            // Search Customer Based on Cnic and mobile
            // if exist --> Do Disbursement
            // else--> Marked Status Parked
            Customer customer = daoFactory.getCustomCustomerDao().fetchCustomerByCnicMobileAndType(cnic, mobile,
                    orgUnitId, customerType, CustomConstants.IDENTITY_TYPE_CNIC);

            if (customer != null) {
                if (customer.getStrSpare10() != null && customerL2.getStrSpare10() != null && customer.getStrSpare10().equals(customerL2.getStrSpare10())) {
                    if (customer.getBlacklistReason() == CustomConstants.BLACK_LIST_ACTIVE) {
                        if (customer.getBolSpare5() == null || !customer.getBolSpare5()) {
                            if (disbursementAmount > CustomConstants.INT_ZERO) {

                                MakerChecker requestBean = new MakerChecker();
                                requestBean.setSpareString7(customer.getId().toString());
                                requestBean.setModuleId(CustomConstants.WORK_ITEM_MODULE_CODE_ACCOUNT_CLOSURE);
                                requestBean.setOrgUnitId(CustomConstants.ORG_UNIT);
                                requestBean.setStatusId(CustomConstants.WORK_ITEM_STATUS_CODE_PENDING);
                                List<MakerChecker> list = this.daoFactory.getWorkItemDAO().searchWorkItem(requestBean);

                                if (list == null) {

                                    LimitCheckBean limitCheckBean = customLimitLogic.limitCheck(CustomConstants.USE_CASE_BULK_DISBURSEMENT, customer.getId(), disbursementAmount, true, callerId);
                                    if (limitCheckBean.getErrorCode() != 0) {
                                        if (ErrorConstants.ERROR_PAYER_SINGLE_LIMIT == limitCheckBean.getErrorCode()) {

                                            bulkDisbursementSummaryRes.setComments(StatusCodes.ERROR_EXTERNAL_TRANSACTION_SINGLE_MINIMUM_UNDERRUN_MSG);
                                            //bulkDisbursementSummaryRes.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
                                            daoFactory.getBulkDisbursementSummaryDao().saveOrUpdate(bulkDisbursementSummaryRes, callerId);

                                        } else {

                                            bulkDisbursementSummaryRes.setComments(utilityLogic.fetchStatus(limitCheckBean.getErrorCode(), new Object[]{}).getValue());
                                            //bulkDisbursementSummaryRes.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
                                            daoFactory.getBulkDisbursementSummaryDao().saveOrUpdate(bulkDisbursementSummaryRes, callerId);
                                        }
                                    } else {
                                        amountForTransferIn = amountForTransferIn + disbursementAmount;
                                    }

                                } else {
                                    LOG.error("Cannot perform transaction. Account is pending to Close");
                                    bulkDisbursementSummaryRes.setComments("Cannot perform transaction. Customer account is pending to Close");
                                    //bulkDisbursementSummaryRes.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
                                    daoFactory.getBulkDisbursementSummaryDao().saveOrUpdate(bulkDisbursementSummaryRes, callerId);
                                }
                            } else {
                                LOG.error("Amount should be greater than zero");
                                bulkDisbursementSummaryRes.setComments("Amount should be greater than zero");
                                //bulkDisbursementSummaryRes.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
                                daoFactory.getBulkDisbursementSummaryDao().saveOrUpdate(bulkDisbursementSummaryRes, callerId);
                            }
                        } else {
                            LOG.error("Customer is suspended");
                            bulkDisbursementSummaryRes.setComments("Customer is suspended");
                            //bulkDisbursementSummaryRes.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
                            daoFactory.getBulkDisbursementSummaryDao().saveOrUpdate(bulkDisbursementSummaryRes, callerId);
                        }
                    } else {

                        if (customer.getBlacklistReason() == 9) {
                            LOG.error("Customer is Dormant");
                            bulkDisbursementSummaryRes.setComments("Customer is Dormant");
                           // bulkDisbursementSummaryRes.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
                            daoFactory.getBulkDisbursementSummaryDao().saveOrUpdate(bulkDisbursementSummaryRes, callerId);
                        } else {
                            LOG.error("Customer is Blacklisted");
                            bulkDisbursementSummaryRes.setComments("Customer is Blacklisted");
                           // bulkDisbursementSummaryRes.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
                            daoFactory.getBulkDisbursementSummaryDao().saveOrUpdate(bulkDisbursementSummaryRes, callerId);
                        }
                    }
                } else {
                    LOG.error("Payroll Account must be associated with Corporate Account");
                    bulkDisbursementSummaryRes.setComments("Payroll Account must be associated with Corporate Account");
                    //bulkDisbursementSummaryRes.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
                    daoFactory.getBulkDisbursementSummaryDao().saveOrUpdate(bulkDisbursementSummaryRes, callerId);
                }

            } else {
                parkRec++;
                LOG.error("Payroll Customer Not Found Or Customer Is Inactive");
                LOG.debug("Make Record As Parked.");
                bulkDisbursementSummaryRes.setComments("Payroll Customer Not Found Or Customer Is Inactive");
               //bulkDisbursementSummaryRes.setStatusId(CustomConstants.BULK_REC_STATUS_REJECT);
                daoFactory.getBulkDisbursementSummaryDao().saveOrUpdate(bulkDisbursementSummaryRes, callerId);

            }
        }
        /* need to remove this code but confirm because someone has update the event parkedRecords are not
        final Integer parkedRecord = parkedRecords;

		if (parkedRecord > 0) {
			BulkFile bulkFile = daoFactory.getBulkFileDao().getById(fileId);
			bulkFile.setStatusId(CustomConstants.BULK_FILE_STATUS_IN_PROGRESS);
			daoFactory.getBulkFileDao().saveOrUpdate(bulkFile, fileId);
		}*/

        return amountForTransferIn;
    }


    /*@Override
    public SearchBulkDisbursementSummaryResponse fetchBulkDisbursementFileSummaryList(
            SearchBulkDisbursementSummaryRequest request) throws BulkDisbursementException {
        return null;
    }*/
    public void setTransactionTemplate(TransactionOperations transactionTemplate) {
        this.transactionTemplate = transactionTemplate;
    }

    public DaoFactory getDaoFactory() {
        return this.daoFactory;
    }

    public void setDaoFactory(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    public com.sybase365.mobiliser.money.persistence.dao.factory.api.DaoFactory getMoneyDaoFactory() {
        return moneyDaoFactory;
    }

    public void setMoneyDaoFactory(com.sybase365.mobiliser.money.persistence.dao.factory.api.DaoFactory moneyDaoFactory) {
        this.moneyDaoFactory = moneyDaoFactory;
    }

    public IUtilityLogic getUtilityLogic() {
        return utilityLogic;
    }

    public void setUtilityLogic(IUtilityLogic utilityLogic) {
        this.utilityLogic = utilityLogic;
    }

    public IBulkDisbursementSummaryConverter getBulkDisbursementSummaryConverter() {
        return bulkDisbursementSummaryConverter;
    }

    public void setBulkDisbursementSummaryConverter(IBulkDisbursementSummaryConverter bulkDisbursementSummaryConverter) {
        this.bulkDisbursementSummaryConverter = bulkDisbursementSummaryConverter;
    }

    public EventGenerator getEventGenerator() {
        return eventGenerator;
    }

    public void setEventGenerator(EventGenerator eventGenerator) {
        this.eventGenerator = eventGenerator;
    }

    public void setTransactionsService(ICustomTransactionEndpoint transactionsService) {
        this.transactionsService = transactionsService;
    }

    public void setCustomTransactionUtil(CustomTransactionUtil customTransactionUtil) {
        this.customTransactionUtil = customTransactionUtil;
    }

    public void setCustomUtil(CustomUtil customUtil) {
        this.customUtil = customUtil;
    }

    public void setCustomerUtil(CustomerUtil customerUtil) {
        this.customerUtil = customerUtil;
    }

//	public void setMakerCheckerLogic(IMakerCheckerLogic makerCheckerLogic) {
//		this.makerCheckerLogic = makerCheckerLogic;
//	}

    public void setBulkFileConverter(IBulkFileConverter bulkFileConverter) {
        this.bulkFileConverter = bulkFileConverter;
    }

//	public void setCallerUtils(ICallerUtils callerUtils) {
//		this.callerUtils = callerUtils;
//	}

    public void setMakerCheckerConverter(MakerCheckerConverter makerCheckerConverter) {
        this.makerCheckerConverter = makerCheckerConverter;
    }

    public CustomLimitLogicImpl getCustomLimitLogic() {
        return customLimitLogic;
    }

    public void setCustomLimitLogic(CustomLimitLogicImpl customLimitLogic) {
        this.customLimitLogic = customLimitLogic;
    }
}
